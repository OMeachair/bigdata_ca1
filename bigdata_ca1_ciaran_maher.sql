create table movies (id serial primary key, title text, writer text, release_year int, franchise text, synopsis text);  
create table actors (id serial primary key, name text);
create table movies_actors (movie_id int references movies(id), actor_id int references actors(id));

/*2. Insert Documents*/
insert into movies (title, writer, release_year) values ('Fight Club','Chuck Palahniuk',1999); 
insert into movies (title, writer, release_year) values ('Pulp Fiction','Quentin Tarantino',1994); 
insert into movies (title, writer, release_year) values ('Inglorious Basterds','Quentin Tarantino',2009); 
insert into movies (title, writer, release_year, franchise) values ('The Hobbit: An Unexpected Journey','J.R.R. Tolkien',2012,'The Hobit'); 
insert into movies (title, writer, release_year, franchise) values ('The Hobbit: The Desolation of Smaug','J.R.R. Tolkien',2013,'The Hobit'); 
insert into movies (title, writer, release_year, franchise, synopsis) values ('The Hobbit: The Battle of the Five Armies','J.R.R. Tolkien',2014,'The Hobit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.'); 
insert into movies (title) values ('Pee Wee Herman''s Big Adventure');
insert into movies (title) values ('Avatar');

insert into actors ( name) values ('Brad Pitt');
insert into actors ( name) values ('Edward Norton');
insert into actors ( name) values ('John Travolta');
insert into actors ( name) values ('Uma Thurman');
insert into actors ( name) values ('Diane Kruger');
insert into actors ( name) values ('Eli Roth');

insert into movies_actors (movie_id, actor_id) values (1,1);
insert into movies_actors (movie_id, actor_id) values (1,2);
insert into movies_actors (movie_id, actor_id) values (2,3);
insert into movies_actors (movie_id, actor_id) values (2,4);
insert into movies_actors (movie_id, actor_id) values (3,1);
insert into movies_actors (movie_id, actor_id) values (3,5);
insert into movies_actors (movie_id, actor_id) values (3,6);

/*3. Query Documents*/
select a.name,m.title,m.writer,m.release_year,m.franchise from movies m left outer join movies_actors ma on m.id = ma.movie_id left outer join actors a on a.id = ma.actor_id;
select * from movies where writer='Quentin Tarantino';
select a.name,m.title,m.writer,m.release_year,m.franchise, from movies m left outer join movies_actors ma on m.id = ma.movie_id left outer join actors a on a.id = ma.actor_id where a.name='Brad Pitt';
select * from movies where franchise='The Hobbit';


/*4. Update Documents*/
update movies set synopsis = 'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.' where title='The Hobbit: An Unexpected Journey';
update movies set synopsis = 'The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.' where title='The Hobbit: The Desolation of Smaug';

/*5. Text Search*/
select title, synopsis from movies where synopsis ilike '%Bilbo%';
select title, synopsis from movies where synopsis ilike '%Gandalf%';
select title, synopsis from movies where synopsis ilike '%Bilbo%' and synopsis not ilike '%Gandalf%';
select title, synopsis from movies where synopsis ilike '%dwarves%' or synopsis ilike '%hobbits%';
select title, synopsis from movies where synopsis ilike '%gold%' and synopsis ilike '%dragon%';

/*6. Delete Documents*/
delete from movies where title = 'Pee Wee Herman''s Big Adventure';
delete from movies where title = 'Avatar';

/*7. Relationships*/
create table users (id serial primary key, username text, firstname text, lastname text);  

insert into users (username, firstname, lastname) values ('GoodGuyGreg','Good Guy','Greg');
insert into users (username, firstname, lastname) values ('ScumbagSteve','Scumbag','Steve');

create table posts (id serial primary key, username text, title text, body text); 

insert into posts (username, title, body) values ('GoodGuyGreg','Passes out at party','Wakes up early and cleans house');
insert into posts (username, title, body) values ('GoodGuyGreg','Steals your identity','Raises your credit score');
insert into posts (username, title, body) values ('GoodGuyGreg','Reports a bug in your code','Sends you a Pull Request');
insert into posts (username, title, body) values ('ScumbagSteve','Borrows something','Sells it');
insert into posts (username, title, body) values ('ScumbagSteve','Borrows everything','The end');
insert into posts (username, title, body) values ('ScumbagSteve','Forks your repo on github','Sets to private');

create table users_posts (user_id int references users(id), post_id int references posts(id)); 

insert into users_posts (user_id, post_id) values (1,1);
insert into users_posts (user_id, post_id) values (1,2);
insert into users_posts (user_id, post_id) values (1,3);
insert into users_posts (user_id, post_id) values (2,4);
insert into users_posts (user_id, post_id) values (2,5);
insert into users_posts (user_id, post_id) values (2,6);

create table comments (id serial primary key, username text, comment text, post_id int references posts(id)); 

insert into comments (username, comment, post_id) values ('GoodGuyGreg','Hope you got a good deal!',4);
insert into comments (username, comment, post_id) values ('GoodGuyGreg','What''s mine is yours!',5);
insert into comments (username, comment, post_id) values ('GoodGuyGreg','Don''t violate the licensing agreement!',6);
insert into comments (username, comment, post_id) values ('ScumbagSteve','It still isn''t clean',1);
insert into comments (username, comment, post_id) values ('ScumbagSteve','Denied your PR cause I found a hack',3);

/*8. Querying Related Collections*/
select * from users;
select * from posts;
select * from posts where username='GoodGuyGreg';
select * from posts where username='ScumbagSteve';
select * from comments;
select * from comments where username='GoodGuyGreg';
select * from comments where username='ScumbagSteve';
select comment from comments where post_id = (select id from posts where title = 'Reports a bug in your code');