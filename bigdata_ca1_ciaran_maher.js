/*2. Insert Documents*/
db.movies.insert({title:"Fight Club",writer:"Chuck Palahniuk",year:1999 ,actors:["Brad Pitt","Edward Norton"]});
db.movies.insert({title:"Pulp Fiction",writer:"Quentin Tarantino",year:1994 ,actors:["John Travolta","Uma Thurman"]});
db.movies.insert({title:"Inglorious Basterds",writer:"Quentin Tarantino",year:2009,actors:["Brad Pitt","Diane Kruger","Eli Roth"]});
db.movies.insert({title:"The Hobbit: An Unexpected Journey",writer:"J.R.R Tolkein",year:2012,franchise:"The Hobbit"});
db.movies.insert({title:"The Hobbit: The Desolation of Smaug",writer:"J.R.R Tolkein",year:2013,franchise:"The Hobbit"});
db.movies.insert({title:"The Hobbit: The Battle of the Five Armies",writer:"J.R.R Tolkein",year:2014,franchise:"The Hobbit",synopsis:"Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."});
db.movies.insert({title:"Pee Wee Herman's Big Adventure"});
db.movies.insert({title:"Avatar"});

/*3. Query Documents*/
db.movies.find().pretty()
db.movies.find({"writer":"Quentin Tarantino"}).pretty()
db.movies.find({"actors":"Brad Pitt"}).pretty()
db.movies.find({"franchise":"The Hobbit"}).pretty()
db.movies.find({"year":{$gt:1989,$lt:2000}}).pretty();
db.movies.find({$or:[{"year":{$lt:2000}},{"year":{$gt:2010}}]}).pretty();

/*4. Update Documents*/
db.movies.update({"title" : "The Hobbit: An Unexpected Journey"},{
	$set:{
		"synopsis" : "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug."
	}
});

db.movies.update({"title" : "The Hobbit: The Desolation of Smaug"},{
	$set:{
		"synopsis" : "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."
	}
});

db.movies.update({"title" : "Pulp Fiction"},{
	$push:{
		"actors":"Samuel L. Jackson"
	}
});

/*5. Text Search*/
db.movies.find({"synopsis":/Bilbo/}).pretty()
db.movies.find({"synopsis":/Gandalf/}).pretty()
db.movies.find({$and:[{"synopsis":/Bilbo/},{"synopsis":{$not:/Gandalf/}}]}).pretty()
db.movies.find({$or:[{"synopsis":/dwarves/},{"synopsis":/hobbit/}]}).pretty()
db.movies.find({$and:[{"synopsis":/gold/},{"synopsis":/dragon/}]}).pretty()

/*6. Delete Documents*/
db.movies.remove({"title":"Pee Wee Herman's Big Adventure"})
db.movies.remove({"title":"Avatar"})

/*7. Relationships*/
db.users.insert({
		username : "GoodGuyGreg",
		first_name : "Good Guy",
		last_name : "Greg",
});
db.users.insert({
		username : "ScumbagSteve",
		fullname : { 
			first : "Scumbag",
			last : "Steve"
		}
});

newPost("GoodGuyGreg","Passes out at party","Wakes up early and cleans house");
newPost("GoodGuyGreg","Steals your identity","Raises your credit score");
newPost("GoodGuyGreg","Reports a bug in your code","Sends you a Pull Request");
newPost("ScumbagSteve","Borrows something","Sells it");
newPost("ScumbagSteve","Borrows everything","The end");
newPost("ScumbagSteve","Forks your repo on github","Sets to private");

newComment("GoodGuyGreg","Hope you got a good deal!",new ObjectId("59e8ffea66da44ec73db8769"));
newComment("GoodGuyGreg","What's mine is yours!",new ObjectId("59e8ffea66da44ec73db876a"));
newComment("GoodGuyGreg","Don't violate the licensing agreement!",new ObjectId("59e8ffeb66da44ec73db876b"));
newComment("ScumbagSteve","It still isn't clean",new ObjectId("59e8ffea66da44ec73db8766"));
newComment("ScumbagSteve","Denied your PR cause I found a hack",new ObjectId("59e8ffea66da44ec73db8768"));

/*8. Querying Related Collections*/
db.users.find().pretty()
db.posts.find().pretty()
db.posts.find({"username":"GoodGuyGreg"}).pretty()
db.posts.find({"username":"ScumbagSteve"}).pretty()
db.comments.find().pretty()
db.comments.find({"username":"GoodGuyGreg"}).pretty()
db.comments.find({"username":"ScumbagSteve"}).pretty()
db.comments.find({"post":ObjectId("59e8ffea66da44ec73db8768")}).pretty()
